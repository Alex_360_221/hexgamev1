// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/InputComponent.h"
#include "PlayerPawn.h"

// Sets default values
APlayerPawn::APlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	springArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	mesh = CreateAbstractDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	//RootComponent->mesh;
	springArm->AttachTo(RootComponent);
	springArm->TargetArmLength = 350.0f;
	springArm->SetWorldRotation(FRotator(-60.0f, 0.0f, 0.0f));

	camera->AttachTo(springArm,USpringArmComponent::SocketName);

	AutoPossessPlayer = EAutoReceiveInput::Player0;
}

// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!currentVelocity.IsZero())
	{
		FVector newLocation = GetActorLocation() + (currentVelocity*DeltaTime);
		SetActorLocation(newLocation);
	}
}

// Called to bind functionality to input
void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	//PlayerInputComponent->BindAction("MoveUp", IE_Pressed, this, &APlayerPawn::MoveUp);
	PlayerInputComponent->BindAction("MoveUp",IE_Pressed, this, &APlayerPawn::MoveUp);
	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerPawn::MoveForward);

}

void APlayerPawn::MoveUp()	
{
	if (Controller)
	{
		AddMovementInput(GetActorForwardVector(),1.0f);
	}
}

void APlayerPawn::MoveForward(float axisValue)
{
	currentVelocity.X = FMath::Clamp(axisValue, -1.0f, 1.0f) * 100;
}

